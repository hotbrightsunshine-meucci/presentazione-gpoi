# Presentazione GPOI

La presentazione è stata realizzata con [Marp](https://marp.app/). E' necessario Node installato sul proprio PC. 
In alternativa, sono disponibili i file PDF e HTML già esportati. 

```sh
npm install
./node_modules/.bin/marp --watch index.md
```

# Ruoli

- **Slide 1-2**;    presentazione degli argomenti (Salanitro),
- **Slide 3-5**;    vincolo di bilancio (Bartolozzi),
- **Slide 6-7**;    spostamento del vincolo di bilancio (Bruni),
- **Slide 8-9**;    mercato (Coppi),
- **Slide 10-12**;  domanda (Cabezas),
- **Slide 13-14**;  spostamento della curva di domanda (Parisio),
- **Slide 15**;     offerta (Consortini),
- **Slide 16**;     conclusione (Salanitro)