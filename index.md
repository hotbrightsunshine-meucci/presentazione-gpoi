---
theme: uncover
marp: true
math: mathjax
footer: "Parisio, Bartolozzi, Coppi, Cabezas, Consortini, Bruni, Salanitro"
---

# Gestione e Progetto
Una presentazione sul programma di Gestione Progetto e  Organizzazione di Impresa del quinto anno.

---

![bg right](https://images.unsplash.com/photo-1589666564459-93cdd3ab856a?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTB8fGVjb25vbWljc3xlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60)

## Argomenti

1. Vincolo di bilancio
2. Mercato
3. Domanda
4. Offerta

---

![bg left:60%](https://images.unsplash.com/photo-1631047085941-a29e9730a7e6?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1184&q=80)

### Il vincolo di bilancio

---

![bg right:33%](https://images.unsplash.com/photo-1526304640581-d334cdbbf45e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80)

Il **vincolo** di bilancio è la rappresentazione dei **panieri** dei beni che il *consumatore* è disposto ad acquistare, in relazione al suo **reddito**.

---

$$
    R = p_1q_1 + p_2q_2
$$

$$
    q_2 = \frac{R - p_1q_1}{p_2}
$$
\-
Il vincolo di bilancio è descritto da una **equazione** di una retta che interseca gli assi $X^+$ e $Y^+$.
\-
$$
y = mx + q
$$

---

![bg left:60%](imgs/grafico-vdb.png)

L'asse $X$ individua il valore di $q_1$, mentre quello $Y$ rappresenta $q_2$.

---

![bg left:60%](imgs/grafico-vdb2.png)

Uno **spostamento** della retta di bilancio indica un cambiamento di **reddito** $R$ a disposizione del consumatore.

--- 

![bg left](https://images.unsplash.com/photo-1569180880150-df4eed93c90b?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8bWFya2V0fGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60)

# Domanda e Mercato

--- 

![bg left:40%](https://images.unsplash.com/photo-1617871772974-26b107fc4c88?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=765&q=80)

### Mercato

Il mercato è il **luogo**, fisico o figurato, nel quale la **domanda** incontra l'**offerta**, definendo il **prezzo** di un bene.  

--- 

### Domanda

![bg right:40%](https://images.unsplash.com/photo-1525328437458-0c4d4db7cab4?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80)

Quantità di beni che un compratore è disposto ad acquistare. 

E' definita in funzione di un **prezzo**.

---

![bg left:40%](https://images.unsplash.com/photo-1609153450348-06f217a09313?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1051&q=80)

### E' anche definita da

- gusti dei consumatori
- reddito dei consumatori
- quantità e prezzo dei beni alternativi o complementari

--- 

![bg contain](http://www.isikeynes.it/ipertesti/mate_eco/IMAGE1.GIF)

![bg contain](http://www.isikeynes.it/ipertesti/mate_eco/IMAGE2.GIF)

---

![bg left](https://images.unsplash.com/photo-1618044733300-9472054094ee?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80)

## Spostamenti della curva di domanda

---

![bg contain](imgs/domanda3.png)

---

![bg right](https://images.unsplash.com/photo-1622760274068-a26adafc984f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80)

## Offerta

La quantità di un certo bene che il produttore è **disposto a vendere** in corrispondenza di un dato prezzo.

---

# Grazie per l'attenzione